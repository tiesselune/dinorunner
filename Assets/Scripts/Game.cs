﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{
	public GameObject cactusPrefab = null;
	public Transform spawnTransform = null;
    // Start is called before the first frame update
    void Start()
    {
		Instantiate(cactusPrefab, spawnTransform.position, spawnTransform.rotation);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
