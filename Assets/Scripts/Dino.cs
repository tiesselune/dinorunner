﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dino : MonoBehaviour
{

	Rigidbody2D rigidBody = null;
	Animator animator = null;
	bool isJumping = false;
    // Start is called before the first frame update
    void Start()
    {
		rigidBody = GetComponent<Rigidbody2D>();
		animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump") && !isJumping)
		{
			isJumping = true;
			animator.SetBool("RunningParameter", true);
			rigidBody.AddForce(new Vector2(0.0f, 400.0f));
		}
    }

	void OnCollisionEnter2D(Collision2D collision)
	{
		if(collision.collider.name == "Ground")
		{
			isJumping = false;
		}
	}
}
